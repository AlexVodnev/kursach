#include <map>
#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

int main()
{
	ifstream file ("/home/alexvd/Документы/Alex/HSE/Kursach/abc.txt");
	long fileLen = 0;
	if (file) 
	{
		file.seekg(0, ios_base::end);
		fileLen = file.tellg();
	}
	
	char *text = new char[fileLen];
	file.seekg(0, ios_base::beg);
	file.getline(text, fileLen, '\0');
	file.close();
	//cout << text;

	map <char, int> chars;
	for (auto i = 0; i < strlen(text); i++)
	{
	    //cout << text[i] << endl;
	    chars[text[i]]++;
    }

	ofstream fout("/home/alexvd/Документы/Alex/HSE/Kursach/Analisys/analisys_text.txt");
	int max = 0;
	for (auto i = chars.begin(); i != chars.end(); i++)
	{
		if ((i -> second) > max)
		{
			max = i -> second;
		}
	}
	fout << max;
	//cout << max;
	fout.close();
	delete [] text;

	return 0;
}