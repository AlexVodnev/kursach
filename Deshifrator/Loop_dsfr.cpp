#include <fstream>
#include <cstring>
#include <cmath>
#include <iostream>
using namespace std;

string wfile(string name)
{
	ifstream file(name);
	long file_Len = 0;
	if (file)
	{
		file.seekg(0, ios_base::end);
		file_Len	= file.tellg();
	}
	
	char *text = new char[file_Len];
	file.seekg(0, ios_base::beg);
	file.getline(text, file_Len + 1, '\0');
	file.close();

	string S;
	for (int i = 0; i < strlen(text); i ++)
	{
		S += text[i];
	}
	delete [] text;
	return(S);
}

int func(int a1, int k)
{
	int a2 = pow(10, k) - 2 * abs(a1 - 5 * pow(10, (k - 1)));
	return a2;
}

int main()
{
	string key_file = "/home/alexvd/Документы/Alex/HSE/Kursach/SendFiles/key.txt";
	string key = wfile(key_file);
	int key_int = atoi(key.c_str());
	
	string len_seq_file = "/home/alexvd/Документы/Alex/HSE/Kursach/SendFiles/len_c.txt";
	string len_seq = wfile(len_seq_file);
	int len_seq_int = atoi(len_seq.c_str());
	
	int dd = len_seq_int + 0.1 * len_seq_int;
	
	int k = (log(len_seq_int / 2)/log(5)) + 1;
	//cout << k;
	int *seq2 = new int[dd];
	seq2[0] = key_int;
	for(int i = 1; i < dd; i++)
	{
		seq2[i] = func(seq2[i - 1], k);	
	}

	int nomerkey = 0;
	while (seq2[nomerkey + len_seq_int] - seq2[nomerkey] != 0)
		nomerkey ++;
	//cout << seq2[nomerkey];

	ofstream file_seq_dsfr("/home/alexvd/Документы/Alex/HSE/Kursach/Deshifrator/main_seq_dsfr.txt");
	
	for (int i = nomerkey; i < len_seq_int + nomerkey; i ++)
	{
		file_seq_dsfr << seq2[i] << ' ';	
	}
	
	file_seq_dsfr.close();


	delete [] seq2;
	return 0;
}