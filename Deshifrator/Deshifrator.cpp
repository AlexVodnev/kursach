#include <fstream>
#include <cstring>
#include <cmath>
#include <iostream>
#include <stdlib.h>

using namespace std;

string wfile(string name)
{
	ifstream file(name);
	long file_Len = 0;
	if (file)
	{
		file.seekg(0, ios_base::end);
		file_Len	= file.tellg();
	}
	
	char *text = new char[file_Len];
	file.seekg(0, ios_base::beg);
	file.getline(text, file_Len + 1, '\0');
	file.close();

	string S;
	for (int i = 0; i < strlen(text); i ++)
	{
		S += text[i];
	}
	delete [] text;
	return(S);
}

template< typename TypeMass > //шаблонизация типа, для того чтобы применять 1 функцию
							  // для переменных разных типов
int size_of_mass(TypeMass mass)
{
	int i = 0;
	while(mass[i])
	{
		i ++;
	}
	return i;
}

int main () 
{
	string len_seq_file = "/home/alexvd/Документы/Alex/HSE/Kursach/SendFiles/len_c.txt";
	string len_seq = wfile(len_seq_file);
	int len_seq_int = atoi(len_seq.c_str());

	string seq_file = "/home/alexvd/Документы/Alex/HSE/Kursach/Deshifrator/main_seq_dsfr.txt";
	string seq = wfile(seq_file);
	string buff;
	int *seq_int_mass = new int[len_seq_int];
	int count = 0;
	for (int i = 0; i < len_seq_int; i++)
	{
		while(seq[count] != ' ')
		{	
			buff += seq[count];
			count ++;
		}
		seq_int_mass[i] = atoi(buff.c_str());
		count ++;
		buff = "";
	}

	string shifrogramma_file = "/home/alexvd/Документы/Alex/HSE/Kursach/SendFiles/final_shifrogramma.txt";
	string shifrogramma = wfile(shifrogramma_file);
	buff = "";
	int size_mass = 0;
	int *shifrogramma_int_mass = (int*) malloc(size_mass);
	count = 0;
	while (shifrogramma[count] != '\0')
	{
		if (shifrogramma[count] != ' ')
		{
			buff += shifrogramma[count];
		}
		else
		{
			size_mass ++;
			shifrogramma_int_mass = (int*) realloc(shifrogramma_int_mass, size_mass * sizeof(int));
			shifrogramma_int_mass[size_mass - 1] = atoi(buff.c_str());
			buff = "";
			
		}
		count++;
	}

	string shablon_file = "/home/alexvd/Документы/Alex/HSE/Kursach/Deshifrator/Shablon_alphavita.txt";
	string shablon = wfile(shablon_file);

	ofstream final_text ("/home/alexvd/Документы/Alex/HSE/Kursach/final_abc.txt");

	char * shifrogramma_char_mass = new char[size_of_mass(shifrogramma_int_mass)];

	size_mass = size_of_mass(shifrogramma_int_mass);
	for(int c = 0; c < size_mass; c++)
	{
		for(int i = 0; i < len_seq_int; i++)
		{
			if (shifrogramma_int_mass[c] == seq_int_mass[i])
			{
				shifrogramma_char_mass[c] = shablon[2*(i % 94)];
				final_text << shifrogramma_char_mass[c];
			}
		}
	}
	final_text.close();
	
	delete [] shifrogramma_char_mass;
	delete [] shifrogramma_int_mass;
	delete [] seq_int_mass;

	return 0;
}