#include <fstream>
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <math.h>
using namespace std;

int func(int a1, int k)
{
	int a2 = pow(10, k) - 2 * abs(a1 - 5 * pow(10, (k - 1)));
	return a2;
}


int main()
{	
	ifstream an_file("/home/alexvd/Документы/Alex/HSE/Kursach/Analisys/analisys_text.txt");
	long an_file_Len = 0;
	if (an_file)
	{
		an_file.seekg(0, ios_base::end);
		an_file_Len = an_file.tellg();
	}
	
	char *an_file_text = new char[an_file_Len];
	an_file.seekg(0, ios_base::beg);
	an_file.getline(an_file_text, an_file_Len + 1, '\0');
	an_file.close();

	int k = (log((94 * atoi(an_file_text)) / 2) / log (5) + 1) + 1;
	int l_s = pow(10, k);
	int st = rand() % (l_s);
	int dlinacickla = 2 * pow(5, (k - 1));
	int dd = dlinacickla + 0.1 * dlinacickla;
	int *seq1 = new int[dd];
	seq1[0] = st;
	//cout << st;
	for (int i = 1; i < (dlinacickla + 0.1 * dlinacickla); i++)
	{
		seq1[i] = func(seq1[i - 1], k);
	}
	int nomerkey = 0;
	while (seq1[nomerkey + dlinacickla] - seq1[nomerkey] !=0)
		nomerkey ++;
	//cout << seq1[nomerkey - 1];
	//cout << seq1[(nomerkey + dlinacickla) - 1];

	ofstream file_key("/home/alexvd/Документы/Alex/HSE/Kursach/SendFiles/key.txt");
	file_key << st;
	//file_key << seq1[nomerkey];
	file_key.close();
	ofstream file_len_c("/home/alexvd/Документы/Alex/HSE/Kursach/SendFiles/len_c.txt");
	file_len_c << dlinacickla;
	file_len_c.close();
	ofstream file_seq("/home/alexvd/Документы/Alex/HSE/Kursach/Shifrator/main_seq.txt");
	
	for (int i = nomerkey; i < dlinacickla + nomerkey; i ++)
	{
		file_seq << seq1[i] << ' ';	
	}
	
	file_seq.close();

	delete [] seq1;
	return 0;
}