#include <fstream>
#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <map>
using namespace std;


string wfile(string name)
{
	ifstream file(name);
	long file_Len = 0;
	if (file)
	{
		file.seekg(0, ios_base::end);
		file_Len	= file.tellg();
	}
	
	char *text = new char[file_Len];
	file.seekg(0, ios_base::beg);
	file.getline(text, file_Len + 1, '\0');
	file.close();

	string S;
	for (int i = 0; i < strlen(text); i ++)
	{
		S += text[i];
	}
	delete [] text;
	return(S);
}


int main()
{
	
	string shablon_file = "/home/alexvd/Документы/Alex/HSE/Kursach/Shifrator/Shablon_alphavita.txt";
	string shablon = wfile(shablon_file);
	int shabLen = shablon.length() + 1;
	
	string an_file = "/home/alexvd/Документы/Alex/HSE/Kursach/Analisys/analisys_text.txt";
	string an_num_s = wfile(an_file);
	int comc_c = atoi(an_num_s.c_str());

	int **mass = new int *[comc_c];
	for (int i = 0; i < comc_c; i++)
	{
		mass[i] = new int[shabLen];
		for (int j = 0; j < (shabLen / 2); j ++)
		{
			mass[i][j] = (i * (shabLen / 2)) + j + 1;
		}
 	}
	string text_file = "/home/alexvd/Документы/Alex/HSE/Kursach/abc.txt";
	string text = wfile(text_file);
	int* shifrotext = new int[text.length()]; 

	for (int elem = 0; elem < text.length(); elem ++)
	{
		for (int j = 0; j < (shabLen / 2); j++)
		{
			if (text[elem] == shablon[2 * j])
			{
				for (int i = 0; i < comc_c; i++)
				{
					if(mass[i][j] != 0)
					{
						shifrotext[elem] = mass[i][j];
						mass[i][j] = 0;
						break;
					}
				}
				break;
			}
		}
	}

	string seq_file = "/home/alexvd/Документы/Alex/HSE/Kursach/Shifrator/main_seq.txt";
	string seq_text = wfile(seq_file);
	string dl_c_file = "/home/alexvd/Документы/Alex/HSE/Kursach/SendFiles/len_c.txt";
	string dl_c_text = wfile(dl_c_file);
	int dl_c = atoi(dl_c_text.c_str());

	int *seq_mass = new int[dl_c];
	string buff;
	int j = 0;
	for (int i = 0; i < dl_c; i ++)
	{
		while (seq_text[j] != ' ')
		{
			buff += seq_text[j];
			j ++; 
		}
		j++;
		seq_mass[i] = atoi(buff.c_str());
		buff = "";
	}
	ofstream final_shifrogramma("/home/alexvd/Документы/Alex/HSE/Kursach/SendFiles/final_shifrogramma.txt");
	
	for (int i = 0; i < text.length() - 1; i++)
	{
		int id = shifrotext[i];
		shifrotext[i] = seq_mass[id - 1];
		final_shifrogramma << shifrotext[i] << ' ';
		
	}
	final_shifrogramma.close();

	delete [] seq_mass;
	for (int i = 0; i < comc_c; i ++)
		delete [] mass[i];
	
	return 0;
}